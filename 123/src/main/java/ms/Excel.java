package ms;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
	
	//Workbook Sheet Row Cell : Top level abstract classes
	
			//XSSF : 2007 >> XSSFWorkbook
			// SSF: 2003 >> SSFWorkbook

	public static void main(String[] args) throws IOException {
		
		System.out.println("hi");
		
		FileInputStream fis = new FileInputStream("C:\\Users\\sumeetk\\workspace\\123\\data\\dataSheet.xlsx");	
		
		Workbook wb = new XSSFWorkbook(fis);
		Sheet sheet = wb.getSheetAt(0);
		System.out.println(sheet.getSheetName());
		
		Iterator<Row> row_itr = sheet.iterator();
		
		row_itr.next();//0 ke pehle 
		
		while(row_itr.hasNext()){
			
			Row row_line = 	row_itr.next();
			
			Iterator<Cell> cell_itr = row_line.cellIterator();
			
			while(cell_itr.hasNext()){
				
				Cell target_cell = cell_itr.next();
				System.out.println(target_cell.getStringCellValue());
			}
			
				
		}
				
		if(fis != null){
			
			fis.close();
		}	
		

	}
}
